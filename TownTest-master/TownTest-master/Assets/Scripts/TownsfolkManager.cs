﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownsfolkManager : MonoBehaviour
{
    public static GameObject ghost;
    public static GameObject baker;
    public static GameObject priest;
    public static GameObject miner;
    public static GameObject smith;
    public static GameObject jack;
    public static GameObject farmA;
    public static GameObject farmB;

    public static bool loaded;

    private void Awake()
    {
        loaded  = false                          ;
        ghost   = GameObject.Find("Ghost"       );
        baker   = GameObject.Find("Baker"       );
        priest  = GameObject.Find("Priest"      );
        miner   = GameObject.Find("Miner"       );
        smith   = GameObject.Find("Blacksmith"  );
        jack    = GameObject.Find("Lumberjack"  );
        farmA   = GameObject.Find("Farmer A"    );
        farmB   = GameObject.Find("Farmer B"    );
        loaded  = true                           ;
    }
}
