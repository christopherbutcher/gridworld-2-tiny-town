﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using Panda;

public class PandaBTScripts : MonoBehaviour
{
    public GameObject body;

    LightingManager liteMGMT;

    NavMeshAgent agent;
    GameObject[] waypoints;
    GameObject curDest;

    GameObject home;
    GameObject work;
    GameObject shop;

    float currentToolCondition = 100.0f; 

    public Dictionary<string, int> resources = new Dictionary<string, int>()
    {
        {"ORE", 0}, {"WOOD", 0}, {"BREAD", 0 }, {"TOOLS", 1}, {"VEG", 0}, {"WATER", 0}
    };

    void Start()
    {
        liteMGMT = FindObjectOfType<LightingManager>();

        while (!LocationManager.loaded && !TownsfolkManager.loaded) ;

        agent = this.GetComponent<NavMeshAgent>();
        agent.stoppingDistance = 0;
        waypoints = GameObject.FindGameObjectsWithTag("waypoint");
        if (waypoints.Length <= 0) Debug.Log("No waypoints found!");

        DetermineProfession();
    }

    void DetermineProfession()
    {
        // 7sdNRVX2VL349jPS22oqLW
        switch (name)
        {
            case "Baker":
                work = LocationManager.bakery;
                shop = LocationManager.sell_bread;
                home = LocationManager.house_2;
                resources["BREAD"] += Random.Range(1, 9);
                break;
            case "Miner":
                work = LocationManager.mine;
                shop = LocationManager.sell_ore;
                home = LocationManager.house_3;
                resources["ORE"] += Random.Range(1, 9);
                break;
            case "Blacksmith":
                work = LocationManager.blacksmith;
                shop = LocationManager.sell_tool;
                home = LocationManager.house_4;
                resources["TOOLS"] += Random.Range(1, 9);
                break;
            case "Lumberjack":
                work = LocationManager.forest_B;
                shop = LocationManager.sell_wood;
                home = LocationManager.house_1;
                resources["WOOD"] += Random.Range(1, 9);
                break;
            case "Farmer A":
                work = LocationManager.farm_B;
                shop = LocationManager.sell_veg;
                home = LocationManager.inn;
                resources["VEG"] += Random.Range(1, 9);
                break;
            case "Farmer B":
                work = LocationManager.farm_A;
                shop = LocationManager.sell_veg;
                home = LocationManager.inn2;
                resources["VEG"] += Random.Range(1, 9);
                break;
            case "Priest":
                work = LocationManager.belltower;
                home = LocationManager.stable;
                break;
            case "Ghost":
                break;
            default:
                Debug.LogError("NO SUCH TOWNSPERSON!");
                break;
        }
    }

    [Task]
    public void SetNewRandomDestination()
    {
        Vector3 dest = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));

        curDest = null;
        Vacate();
        agent.SetDestination(dest);
        Task.current.Succeed();
    }

    [Task]
    public void MoveToNewDestination()
    {
        if (!body.activeInHierarchy)
        {
            body.SetActive(true);
        }

        if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
        {
            //Debug.Log(name + " " + agent.destination + ":" + agent.stoppingDistance + " (" + agent.remainingDistance + ")");
            Task.current.Succeed();
        }
    }

    [Task]
    public void GetMaterials()
    {
        switch (name)
        {
            case "Baker":
                resources["BREAD"] += Random.Range(1, 9);
                break;
            case "Miner":
                resources["ORE"] += Random.Range(1, 9);
                break;
            case "Blacksmith":
                resources["TOOLS"] += Random.Range(1, 9);
                break;
            case "Lumberjack":
                resources["WOOD"] += Random.Range(1, 9);
                break;
            case "Farmer A":
            case "Farmer B":
                resources["VEG"] += Random.Range(1, 9);
                break;
            default:
                Debug.LogError("NO SUCH TOWNSPERSON!");
                break;
        }

        currentToolCondition -= Random.Range(0.0f, 100.0f);

        if(currentToolCondition <= 0)
        {
            resources["TOOLS"]--;
            Debug.Log(name + " has broken their tools!");
            currentToolCondition = 100f;
        }

        Debug.Log(name + " now has more resources!");
        Task.current.Succeed();
    }

    [Task]
    public void LeaveForWork()
    {
        if (work.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else
        {
            work.GetComponent<Waypoint>().occupant = this.gameObject;
            curDest = work;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " has left for work.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void GoHome()
    {
        if(home.GetComponent<Waypoint>().occupant != null)
        {
            Debug.Log("GO HOME FAILED FOR " + name);
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = home;
            //Debug.Log(home.name + "== HOME DOT NAME FOR " + name);
            home.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform.position);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            //Debug.Log("DEST: " + dest + " for " + name);
            Debug.Log(name + " is heading home.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void GoToChurch()
    {
        if(LocationManager.church.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.church;
            LocationManager.church.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            GetComponent<Commerce>().Tithe();
            Debug.Log(name + " is heading to church.");
            Task.current.Succeed();
        }
        
    }

    [Task]
    public void ShopForWood()
    {
        if(LocationManager.buy_wood.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else if(this.gameObject == TownsfolkManager.jack)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.buy_wood;
            LocationManager.buy_wood.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is shopping for wood.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void ShopForVeg()
    {
        if (LocationManager.buy_veg.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else if (this.gameObject == TownsfolkManager.farmA || this.gameObject == TownsfolkManager.farmB)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.buy_veg;
            LocationManager.buy_veg.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is shopping for veggies.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void ShopForOre()
    {
        if (LocationManager.buy_ore.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else if (this.gameObject == TownsfolkManager.miner)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.buy_ore;
            LocationManager.buy_ore.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is shopping for ore.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void ShopForTools()
    {
        if (LocationManager.buy_tool.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else if (this.gameObject == TownsfolkManager.smith)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.buy_tool;
            LocationManager.buy_tool.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is shopping for tools.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void ShopForBread()
    {
        if (LocationManager.buy_bread.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else if (this.gameObject == TownsfolkManager.baker)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            curDest = LocationManager.buy_bread;
            LocationManager.buy_bread.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is shopping for bread.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void BuyWood()
    {
        if (!TownsfolkManager.jack.GetComponent<PandaBTScripts>().IsAtShop())
        {
            Task.current.Fail();
        }
        else
        {
            if (!GetComponent<Commerce>().HasEnough(Commerce.wood_price))
            {
                Task.current.Fail();
            }
            else
            {
                TownsfolkManager.jack.GetComponent<Commerce>().Sold("WOOD", this.GetComponent<Commerce>());
                Task.current.Succeed();
            }
        }
    }

    [Task]
    public void BuyTools()
    {
        if (!TownsfolkManager.smith.GetComponent<PandaBTScripts>().IsAtShop())
        {
            Task.current.Fail();
        }
        else
        {
            if (!GetComponent<Commerce>().HasEnough(Commerce.tool_price))
            {
                Task.current.Fail();
            }
            else
            {
                TownsfolkManager.smith.GetComponent<Commerce>().Sold("TOOLS", this.GetComponent<Commerce>());
                Task.current.Succeed();
            }
        }
    }

    [Task]
    public void BuyBread()
    {
        if (!TownsfolkManager.baker.GetComponent<PandaBTScripts>().IsAtShop())
        {
            Task.current.Fail();
        }
        else
        {
            if (!GetComponent<Commerce>().HasEnough(Commerce.bread_price))
            {
                Task.current.Fail();
            }
            else
            {
                TownsfolkManager.baker.GetComponent<Commerce>().Sold("BREAD", this.GetComponent<Commerce>());
                Task.current.Succeed();
            }
        }
    }

    [Task]
    public void BuyVeg()
    {
        if (!TownsfolkManager.farmA.GetComponent<PandaBTScripts>().IsAtShop() &&
            !TownsfolkManager.farmB.GetComponent<PandaBTScripts>().IsAtShop())
        {
            Task.current.Fail();
        }
        else
        {
            if (!GetComponent<Commerce>().HasEnough(Commerce.veg_price))
            {
                Task.current.Fail();
            }
            else
            {
                if (TownsfolkManager.farmA.GetComponent<PandaBTScripts>().IsAtShop())
                {
                    TownsfolkManager.farmA.GetComponent<Commerce>().Sold("VEG", this.GetComponent<Commerce>());
                    Task.current.Succeed();
                }
                else
                {
                    TownsfolkManager.farmB.GetComponent<Commerce>().Sold("VEG", this.GetComponent<Commerce>());
                    Task.current.Succeed();
                }
                
            }
        }
    }

    [Task]
    public void BuyOre()
    {
        if (!TownsfolkManager.miner.GetComponent<PandaBTScripts>().IsAtShop())
        {
            Task.current.Fail();
        }
        else
        {
            if (!GetComponent<Commerce>().HasEnough(Commerce.ore_price))
            {
                Task.current.Fail();
            }
            else
            {
                TownsfolkManager.miner.GetComponent<Commerce>().Sold("ORE", this.GetComponent<Commerce>());
                Task.current.Succeed();
            }
        }
    }

    [Task]
    public void GoToWell()
    {
        if(LocationManager.well_A.GetComponent<Waypoint>().occupant == null)
        {
            Vacate();
            curDest = LocationManager.well_A;
            curDest.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is getting some water.");
            Task.current.Succeed();
        }
        else if(LocationManager.well_B.GetComponent<Waypoint>().occupant == null)
        {
            Vacate();
            curDest = LocationManager.well_B;
            curDest.GetComponent<Waypoint>().occupant = this.gameObject;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is getting some water.");
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void GetWater()
    {
        resources["WATER"]++;
        Task.current.Succeed();
    }

    [Task]
    public void GoToStore()
    {
        // 0h5cHAx4g0l3ppyxK6OYtd
        if (shop.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            shop.GetComponent<Waypoint>().occupant = this.gameObject;
            curDest = shop;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Debug.Log(name + " is heading to their shop.");
            Task.current.Succeed();
        }
    }

    [Task]
    public void OpenForBusiness()
    {
        curDest = shop;
        Task.current.Succeed();
    }

    [Task]
    public void ChangeColor()
    {
        GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        Task.current.Succeed();
    }

    [Task]
    public void EnterBuilding()
    {
        body.SetActive(false);
        Vacate();
        Task.current.Succeed();
    }

    [Task]
    public void LeaveBuilding()
    {
        body.SetActive(true);
        Vacate();
        Task.current.Succeed();
    }

    [Task]
    public void HasBread()
    {
        if (resources["BREAD"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasVeg()
    {
        if (resources["VEG"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasTools()
    {
        if (resources["TOOLS"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasOre()
    {
        if (resources["ORE"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasWood()
    {
        if (resources["WOOD"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasWater()
    {
        if (resources["WATER"] > 1)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void IsDaylight()
    {
        if (liteMGMT.WhatTimeIsIt() >= 6 &&
            liteMGMT.WhatTimeIsIt() <= 18)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    void Vacate()
    {
        if (curDest != null)
        {
            curDest.GetComponent<Waypoint>().occupant = null;
        }
    }

    public bool IsAwake()
    {
        if(body.activeInHierarchy == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [Task]
    public void HasResources()
    {
        bool result = false;
        switch (name)
        {
            case "Baker":
                if(resources["BREAD"] > 1)
                {
                    result = true;
                }
                break;
            case "Miner":
                if(resources["ORE"] > 1)
                {
                    result = true;
                }
                break;
            case "Blacksmith":
                if(resources["TOOLS"] > 1)
                {
                    result = true;
                }
                break;
            case "Lumberjack":
                if (resources["WOOD"] > 1)
                {
                    result = true;
                }
                break;
            case "Farmer A":
            case "Farmer B":
                if(resources["VEG"]  > 1)
                {
                    result = true;
                }
                break;
            default:
                Debug.LogError("NO SUCH TOWNSPERSON!");
                break;
        }
        if (result)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void HasEnough()
    {
        bool result = false;
        switch (name)
        {
            case "Baker":
                if (resources["BREAD"] >= 10)
                {
                    result = true;
                }
                break;
            case "Miner":
                if (resources["ORE"] >= 10)
                {
                    result = true;
                }
                break;
            case "Blacksmith":
                if (resources["TOOLS"] >= 10)
                {
                    result = true;
                }
                break;
            case "Lumberjack":
                if (resources["WOOD"] >= 10)
                {
                    result = true;
                }
                break;
            case "Farmer A":
            case "Farmer B":
                if (resources["VEG"] >= 10)
                {
                    result = true;
                }
                break;
            default:
                Debug.LogError("NO SUCH TOWNSPERSON!");
                break;
        }
        if (result)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    public bool IsAtShop()
    {
        if(curDest == shop)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [Task]
    public void IsSunday()
    {
        if (LightingManager.IsSunday()) {Task.current.Succeed();}
        else { Task.current.Fail(); }
    }

    [Task]
    public void Consume()
    {
        if (resources["WOOD"] > 0 && resources["VEG"] > 0 && resources["BREAD"] > 0)
        {
            resources["WOOD"]--;
            resources["VEG"]--;
            resources["BREAD"]--;
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void BakerToField()
    {
        if (LocationManager.farm_C.GetComponent<Waypoint>().occupant != null)
        {
            Task.current.Fail();
        }
        else
        {
            Vacate();
            LocationManager.farm_C.GetComponent<Waypoint>().occupant = this.gameObject;
            curDest = LocationManager.farm_C;
            agent.transform.LookAt(curDest.transform);
            Vector3 dest = curDest.transform.position;
            agent.SetDestination(dest);
            Task.current.Succeed();
        }
    }

    [Task]
    public void FieldWork()
    {
        Vacate();
        float tempX = Random.Range(20.5f, 29.5f);
        float tempZ = 0;
        switch (name)
        {
            case "Farmer A":
                tempZ = Random.Range(10.5f, 19.5f);
                break;
            case "Farmer B":
                tempZ = Random.Range(0.5f, 9.5f);
                break;
            case "Baker":
                tempZ = Random.Range(-9.5f, -0.5f);
                break;
            default:
                Task.current.Fail();
                break;
        }

        Vector3 dest = new Vector3(tempX, transform.position.y, tempZ);
        agent.transform.LookAt(dest);
        agent.SetDestination(dest);
        //Debug.Log("***-" + dest);
        Task.current.Succeed();
    }

    [Task]
    public void SwapEquipment()
    {
        if(this.gameObject.name == "Farmer A")
        {
            PandaBTScripts other = TownsfolkManager.farmB.GetComponent<PandaBTScripts>();
            other.resources["TOOLS"] += this.resources["TOOLS"];
        }
        else
        {
            PandaBTScripts other = TownsfolkManager.farmA.GetComponent<PandaBTScripts>();
            other.resources["TOOLS"] += this.resources["TOOLS"];
        }
        
        this.resources["TOOLS"] = 0;
        Task.current.Succeed();
    }
}

