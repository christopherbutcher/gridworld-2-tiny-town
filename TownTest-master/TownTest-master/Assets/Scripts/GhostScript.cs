﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;

public class GhostScript : MonoBehaviour
{
    public GameObject[] meshes;
    public GameObject body;

    LightingManager liteMGMT;

    public Light pointLight;

    NavMeshAgent agent;
    GameObject[] waypoints;
    GameObject curDest;

    // Start is called before the first frame update
    void Start()
    {
        liteMGMT = FindObjectOfType<LightingManager>();

        while (!LocationManager.loaded && !TownsfolkManager.loaded) ;

        agent = this.GetComponent<NavMeshAgent>();
        agent.stoppingDistance = 0;
        waypoints = GameObject.FindGameObjectsWithTag("waypoint");
        if (waypoints.Length <= 0) Debug.Log("No waypoints found!");
    }

    [Task]
    public void FadeOut()
    {
        foreach (GameObject g in meshes)
        {
            Color current = g.GetComponent<MeshRenderer>().material.color;
            g.GetComponent<MeshRenderer>().material.color =
                new Color(current.r, current.g, current.b, (current.a - 0.02f));
            //Debug.Log(g.GetComponent<MeshRenderer>().material.color.a);
        }
        Task.current.Succeed();
    }

    [Task]
    public void FadeIn()
    {
        foreach (GameObject g in meshes)
        {
            Color current = g.GetComponent<MeshRenderer>().material.color;
            g.GetComponent<MeshRenderer>().material.color =
                new Color(current.r, current.g, current.b, (current.a + 0.02f));
            //Debug.Log(g.GetComponent<MeshRenderer>().material.color.a);
        }
        Task.current.Succeed();
    }

    [Task]
    public void ChangeGlow()
    {
        pointLight.enabled = !pointLight.enabled;
        Task.current.Succeed();
    }

    [Task]
    public void Appear()
    {
        body.SetActive(true);
        Task.current.Succeed();
    }

    [Task]
    public void Disappear()
    {
        body.SetActive(false);
        Task.current.Succeed();
    }

    [Task]
    public void Teleport()
    {
        transform.position = LocationManager.graveyard.transform.position;
        Task.current.Succeed();
    }

    [Task]
    public void BackToGraveyard()
    {
        curDest = LocationManager.graveyard;
        agent.transform.LookAt(curDest.transform);
        agent.SetDestination(curDest.transform.position);
        Task.current.Succeed();
    }

    [Task]
    public void SetNewRandomDestination()
    {
        Vector3 dest = new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));
        agent.SetDestination(dest);
        Task.current.Succeed();
    }

    [Task]
    public void MoveToNewDestination()
    {
        if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
        {
            Task.current.Succeed();
        }
    }

    [Task]
    public void IsDaylight()
    {
        if (liteMGMT.WhatTimeIsIt() >= 5 &&
            liteMGMT.WhatTimeIsIt() <= 19)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();

        }
    }

    [Task]
    public void IsVisible()
    {
        if (body.activeInHierarchy)
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }
}
