﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
       public static GameObject graveyard;
      public static GameObject farm_A;
     public static GameObject farm_B;
    public static GameObject farm_C;
     public static GameObject mine;
      public static GameObject well_A;
       public static GameObject well_B;
        public static GameObject church;
         public static GameObject belltower;
          public static GameObject house_1;
         public static GameObject house_2;
        public static GameObject house_3;
       public static GameObject house_4;
        public static GameObject bakery;
         public static GameObject sawmill;
          public static GameObject stable;
           public static GameObject blacksmith;
            public static GameObject inn;
             public static GameObject inn2;
            public static GameObject forest_A;
           public static GameObject forest_B;
          public static GameObject forest_C;
         public static GameObject buy_tool;
        public static GameObject buy_bread;
       public static GameObject buy_veg;
        public static GameObject buy_ore;
         public static GameObject buy_wood;
          public static GameObject sell_tool;
         public static GameObject sell_bread;
        public static GameObject sell_veg;
       public static GameObject sell_ore;
      public static GameObject sell_wood;

    public static bool loaded;

    private void Awake()
    {
        loaded = false;

        graveyard   = GameObject.Find("GraveyardWaypoint"   );
        farm_A      = GameObject.Find("FarmfieldWaypoint1"  );
        farm_B      = GameObject.Find("FarmfieldWaypoint2"  );
        farm_C      = GameObject.Find("FarmfieldWaypoint3"  );
        mine        = GameObject.Find("MineEntranceWaypoint");
        well_A      = GameObject.Find("WellWaypoint1"       );
        well_B      = GameObject.Find("WellWaypoint2"       );
        church      = GameObject.Find("ChurchWaypoint"      );
        belltower   = GameObject.Find("BelltowerWaypoint"   );
        house_1     = GameObject.Find("House1Waypoint"      );
        house_2     = GameObject.Find("House2Waypoint"      );
        house_3     = GameObject.Find("House3Waypoint"      );
        house_4     = GameObject.Find("House4Waypoint"      );
        bakery      = GameObject.Find("MillWaypoint"        );
        sawmill     = GameObject.Find("SawmillWaypoint"     );
        stable      = GameObject.Find("StableWaypoint"      );
        blacksmith  = GameObject.Find("BlacksmithWaypoint"  );
        inn         = GameObject.Find("InnWaypoint"         );
        inn2        = GameObject.Find("InnWaypoint2"        );
        forest_A    = GameObject.Find("ForestWaypoint"      );
        forest_B    = GameObject.Find("ForestWaypoint2"     );
        forest_C    = GameObject.Find("ForestWaypoint3"     );
        buy_tool    = GameObject.Find("ToolShopBuy"         );
        buy_bread   = GameObject.Find("BreadShopBuy"        );
        buy_veg     = GameObject.Find("VegShopBuy"          );
        sell_tool   = GameObject.Find("ToolShopSell"        );
        sell_bread  = GameObject.Find("BreadShopSell"       );
        sell_veg    = GameObject.Find("VegShopSell"         );
        buy_wood    = GameObject.Find("WoodShopBuy"         );
        sell_wood   = GameObject.Find("WoodShopSell"        );
        buy_ore     = GameObject.Find("OreShopBuy"          );
        sell_ore    = GameObject.Find("OreShopSell"         );

        loaded = true;
    }
}
