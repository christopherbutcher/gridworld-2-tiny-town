﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commerce : MonoBehaviour
{
    float money;

    public static float wood_price  = 0.7f;
    public static float ore_price   = 3.1f;
    public static float veg_price   = 0.5f;
    public static float bread_price = 0.9f;
    public static float tool_price  = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        money = UnityEngine.Random.Range(10f, 30f);
    }

    public bool HasEnough(float price)
    {
        if(money < price)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Sold(string product, Commerce other)
    {
        float amount = 0;
        switch (product)
        {
            case "BREAD":
                amount = bread_price;
                break;
            case "TOOLS":
                amount = tool_price;
                break;
            case "VEG":
                amount = veg_price;
                break;
            case "ORE":
                amount = ore_price;
                break;
            case "WOOD":
                amount = wood_price;
                break;
            default:
                break;
        }

        other.GetComponent<PandaBTScripts>().resources[product]++;
        other.money -= amount;
        this.GetComponent<PandaBTScripts>().resources[product]--;
        this.money  += amount;
        Debug.Log(other.name + " just bought some " + product + "!");
    }

    public void Tithe()
    {
        float amount = (float)Math.Round(money * .1f, 2);
        money -= amount;
        TownsfolkManager.priest.GetComponent<Commerce>().money += amount;
    }
}
